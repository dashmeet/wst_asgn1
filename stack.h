#define MAX 128

typedef struct istack {
	int arr[MAX];
	int i;
}istack;

void init(istack *);
void ipush(istack *, int);
int ipop(istack *);
int iempty(istack *);
int ifull(istack *);
int itop(istack *);

//-----------cstack-----------

typedef struct cstack {
	char arr[MAX];
	int i;
}cstack;

void cinit(cstack *);
void cpush(cstack *, char);
char cpop(cstack *);
int cempty(cstack *);
int cfull(cstack *);
char ctop(cstack *);
