#define MAX 128
#include "stack.h"

void init(istack *is) {
	is->i = -1;
}

void ipush(istack *is, int a) {
	(is->i)++;
	is->arr[is->i] = a;
}

int ipop(istack *is) {
	int t;
	t = is->arr[is->i];
	(is->i)--;
	return t;
}

int iempty(istack *is) {
	return is->i == -1;
}

int ifull(istack *is) {
	return is->i == MAX;
}

int itop(istack *is) {
	int t;
	t = ipop(is);
	ipush(is, t);
	return t;
}

//---------------- cstack -----------------
void cinit(cstack *cs) {
	cs->i = -1;
}

void cpush(cstack *cs, char a) {
	(cs->i)++;
	cs->arr[cs->i] = a;
}

char cpop(cstack *cs) {
	char t;
	t = cs->arr[cs->i];
	(cs->i)--;
	return t;
}

int cempty(cstack *cs) {
	return cs->i == -1;
}

int cfull(cstack *cs) {
	return cs->i == MAX;
}

char ctop(cstack *cs) {
	char t;
	t = cpop(cs);
	cpush(cs, t);
	return t;
}
