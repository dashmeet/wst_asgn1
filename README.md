This is a code to evaluate infix mathematical expressions with addition, subtraction, multiplication, division, and modulo;
of INTEGERS only. 

Nested bracketed expressions supported.
Negative integers not supported (yet).

Space between every character is required.
Example: ( ( 2 + 3 ) * 4 - 10 ) + 3 * 5
