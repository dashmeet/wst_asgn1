#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "stack.h"

/*
 *readline will read input in 'arr' till a '\n'
 *or till length of the infix expression = 128 
 *and put a '\0' in the end of the array 'arr'
 *and return the number of bytes read.
 */

int readline(char *arr) {
	int i = 0;
	int ch;
	while((ch = getchar()) != '\n' && i < MAX - 1) {
		arr[i] = ch;
		i++;
	}
	arr[i] = '\0';
	return i;
}

/*
 *infix will accept the string 'str'
 *as an expression and return the evaluated value
 *of the infix expression in str.
 *It will also handle errors.
 */

#define OPERAND 10 
#define OPERATOR 20
#define	END	30
#define ERROR	40
#define OBRACKET 50
#define CBRACKET 60
 
enum states { SPC, DIG, OPR, OBRAC, CBRAC, STOP, ERR };

/*
 *getnext() will return the type of next "token"(operands or operators)
 *from "arr" and store the token in "number" or "op" depending on
 *whether it was operand or operator
 *getnext() returns pointers to malloced memory. The caller should free it
 */
 
typedef struct token {
	int type;
	int number;
	char op;
}token;

token *getnext(char *arr, int *reset) {
	static int currstate;
	int nextstate;
	static int i;
	int num;
	token *t = (token *)malloc(sizeof(token));

	if (*reset == 1) {
		i = 0;
		currstate = SPC;
		*reset = 0;
	}

	while(1) {
		switch(arr[i]) {
			case '0': case '1': case '2': case '3':
			case '4': case '5': case '6': case '7':
			case '8': case '9': 
				nextstate = DIG;
				break;
			case '+': case '-': case '*': case '/':
			case '%':
				nextstate = OPR;
				break;
			case '\0':
				nextstate = STOP;
				break;
			case ' ':
				nextstate = SPC;
				break;
			case '(':
				nextstate = OBRAC;
				break;
			case ')':
				nextstate = CBRAC;
				break;
			default:
				nextstate = ERR;
				break;
		}
		switch(currstate) {
			case SPC:
				if(nextstate == DIG)
					num = arr[i] - '0';			
				break;
			case DIG:
				if(nextstate == DIG)
					num = num * 10 + arr[i] - '0';
				else  {
					t->type = OPERAND;
					t->number = num;
					currstate = nextstate;
					return t;
				}
				break;
			case OPR:
				t->type = OPERATOR;
				t->op = arr[i - 1];	
				currstate = nextstate;
				return t;
				break;
			case OBRAC:
				t->type = OBRACKET;
				t->op = arr[i - 1];	
				currstate = nextstate;
				return t;
				break;
			case CBRAC:
				t->type = CBRACKET;
				t->op = arr[i - 1];	
				currstate = nextstate;
				return t;
				break;
			case STOP:
				t->type = END;
				currstate = nextstate;
				return t;
				break;
			case ERR:
				t->type = ERROR;
				currstate = nextstate;
				return t;
				break;
		}
		currstate = nextstate;
		i++;
	}
}

int precedence (char op) {
	if (op == '%')
		return 3;
	if (op == '*' || op == '/')
		return 2;
	if (op == '+' || op == '-')
		return 1;
	return 0;
}

/* infix evaluates the infix expression in 'str' 
 * and returns the value of the expression
 * returns INT_MIN if there was an error in expression 
 */

int infixeval (char *infix) {
	token *t;
	istack is, is2;
	cstack cs, cs2;
	int a, b, m, n, result;
	char x, y;
	int reset = 1;

	init(&is);
	cinit(&cs);
	init(&is2);
	cinit(&cs2);

	t = getnext(infix, &reset);
	if (t->type == END || t->type == ERROR || t->type == CBRACKET)
		return INT_MIN;

	while (1) {
		if (t->type != CBRACKET && t->type != END) {
			if (t->type == OPERAND)
				ipush(&is, t->number);
			else
				cpush(&cs, t->op);
		} else {
			while (1) {
				ipush(&is2, ipop(&is));
				if (!cempty(&cs) && ((x = cpop(&cs)) != '(')) {
					if (!cempty(&cs2)) {
						y = ctop(&cs2);
						a = precedence(x);
						b = precedence(y);
						if (a > b)
							cpush(&cs2, x);
						while (a <= b) {
							y = cpop(&cs2);
							m = ipop(&is2);
							n = ipop(&is2);
							switch(y) {
								case '%':
									result = m % n;
									ipush(&is2, result);
									break;
								case '*':
									result = m * n;
									ipush(&is2, result);
									break;
								case '/':
									result = m / n;
									ipush(&is2, result);
									break;
								case '+':
									result = m + n;
									ipush(&is2, result);
									break;
								case '-':
									result = m - n;
									ipush(&is2, result);
									break;
							}
							if (!cempty(&cs2)) {
								y = ctop(&cs2);
								b = precedence(y);
							} else {
								cpush(&cs2, x);
								break;
							}
						}
					} else
						cpush(&cs2, x);
				} else {
					while(!cempty(&cs2)) {
						x = cpop(&cs2);
						a = ipop(&is2);
						b = ipop(&is2);
						switch(x) {
							case '%':
								result = a % b;
								ipush(&is2, result);
								break;
							case '*':
								result = a * b;
								ipush(&is2, result);
								break;
							case '/':
								result = a / b;
								ipush(&is2, result);
								break;
							case '+':
								result = a + b;
								ipush(&is2, result);
								break;
							case '-':
								result = a - b;
								ipush(&is2, result);
								break;
						}
					}
					ipush(&is, ipop(&is2));
					break;
				}
			}
		}
		if (t->type == END)
			break;
		t = getnext(infix, &reset);	
	}

	return ipop(&is);
}

/* Problem: solve infix expression. 
 * input: infix expression involving integer operands and
 * binary operatiors (+ - * / % )
 * Input may have errors
 * Two operands will be separated by one or more space
 * operands-operators will be separted by zero or more space 
 * Examples:
 * 11 + 23  +  4 * 5 
 * 10 + 20 + 30  
 * 10 + 20 - 30 + 40
 * Output: number (result)
 */ 

int main() {
	char str[MAX];
	int x, ans;
	printf("Enter an infix expression to be evaluated : \n");
	while((x = readline(str))) {
		ans = infixeval(str);
		if(ans == INT_MIN)
			fprintf(stderr, "Error in expression\n");
		else
			fprintf(stdout, "%d\n", ans);
		printf("To evaluate more expressions enter another, or else press enter to exit the program :\n");
	}
	return 0;
}
